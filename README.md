A simple Hello, World! API with nginx and Docker.

# Build
    
    sudo docker build -t nginx_hello_world_api .

# Run

    sudo docker run -p 80:80 -i -t nginx_hello_world_api
